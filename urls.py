# skrypt konfiguracyjny zawierajacy mapowania pomiedzy URL (adres internetowy) a obslugujaca go funkcja Pythona

from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import direct_to_template
from django.conf import settings
from blog.views import blog_list,blog_detail
from blog.views import *
import os.path

from django.contrib import admin
admin.autodiscover()

media_dir = os.path.join(os.path.dirname(__file__),'media')

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'strona.views.home', name='home'),
    # url(r'^strona/', include('strona.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    # (r'^$',main_page),
    url(r"^$",blog_list, name='blog_list'),
    url(r"^blog/(?P<pk>\d+)/$",blog_detail, name='blog_detail'),
    (r'^media/(?P<path>.*)$','django.views.static.serve',{'document_root': media_dir}),
    (r'^admin/',include(admin.site.urls)),
    (r'^register/$',register_page),
    (r'^login/$','django.contrib.auth.views.login'),
    (r'^logout/$',logout_page),
    (r'^comments/', include('django.contrib.comments.urls')),
)

if settings.DEBUG:
	urlpatterns += patterns(
		'django.views.static',
		(r'media/(?P<path>.*)',
		'serve',
		{'document_root': settings.MEDIA_ROOT}), )
